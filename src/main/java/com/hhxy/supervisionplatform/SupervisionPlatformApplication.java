package com.hhxy.supervisionplatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupervisionPlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(SupervisionPlatformApplication.class, args);
    }

}
