package com.hhxy.supervisionplatform.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.hhxy.supervisionplatform.entity.User;
import com.hhxy.supervisionplatform.mapper.UserMapper;
import com.hhxy.supervisionplatform.service.IUserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
}
