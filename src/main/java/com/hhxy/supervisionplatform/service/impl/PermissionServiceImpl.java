package com.hhxy.supervisionplatform.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.hhxy.supervisionplatform.entity.Permission;
import com.hhxy.supervisionplatform.mapper.PermissionMapper;
import com.hhxy.supervisionplatform.service.IPermissionService;
import org.springframework.stereotype.Service;

@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements IPermissionService {
}
