package com.hhxy.supervisionplatform.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.hhxy.supervisionplatform.entity.Role;
import com.hhxy.supervisionplatform.mapper.RoleMapper;
import com.hhxy.supervisionplatform.service.IRoleService;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {
}
