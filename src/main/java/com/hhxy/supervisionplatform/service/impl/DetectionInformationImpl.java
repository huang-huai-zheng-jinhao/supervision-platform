package com.hhxy.supervisionplatform.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hhxy.supervisionplatform.entity.DetectionInformation;
import com.hhxy.supervisionplatform.mapper.DetectionInformationMapper;
import com.hhxy.supervisionplatform.service.IDetectionInformation;

public class DetectionInformationImpl extends ServiceImpl<DetectionInformationMapper, DetectionInformation> implements IDetectionInformation{
}
