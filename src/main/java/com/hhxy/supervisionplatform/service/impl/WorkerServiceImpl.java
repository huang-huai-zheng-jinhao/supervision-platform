package com.hhxy.supervisionplatform.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hhxy.supervisionplatform.entity.Worker;
import com.hhxy.supervisionplatform.mapper.WorkerMapper;
import com.hhxy.supervisionplatform.service.IWorkerService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zjh
 * @since 2023-03-02
 */
@Service
public class WorkerServiceImpl extends ServiceImpl<WorkerMapper, Worker> implements IWorkerService {

}
