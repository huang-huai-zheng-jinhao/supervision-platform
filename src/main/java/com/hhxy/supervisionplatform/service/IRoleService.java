package com.hhxy.supervisionplatform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hhxy.supervisionplatform.entity.Role;


public interface IRoleService extends IService<Role>{
}
