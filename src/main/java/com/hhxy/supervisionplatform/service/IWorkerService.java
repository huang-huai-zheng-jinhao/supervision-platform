package com.hhxy.supervisionplatform.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.hhxy.supervisionplatform.entity.Worker;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zjh
 * @since 2023-03-02
 */
public interface IWorkerService extends IService<Worker> {

}
