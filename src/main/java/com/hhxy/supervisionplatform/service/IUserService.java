package com.hhxy.supervisionplatform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hhxy.supervisionplatform.entity.User;

public interface IUserService extends IService<User> {
}
