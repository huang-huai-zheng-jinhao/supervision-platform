package com.hhxy.supervisionplatform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hhxy.supervisionplatform.entity.DetectionInformation;

public interface IDetectionInformation extends IService<DetectionInformation> {
}
