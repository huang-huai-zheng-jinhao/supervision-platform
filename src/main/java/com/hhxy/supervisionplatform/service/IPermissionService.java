package com.hhxy.supervisionplatform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hhxy.supervisionplatform.entity.Permission;


public interface IPermissionService extends IService<Permission> {
}
