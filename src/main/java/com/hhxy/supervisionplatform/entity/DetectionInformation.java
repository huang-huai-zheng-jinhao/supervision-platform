package com.hhxy.supervisionplatform.entity;


import lombok.Data;


@Data
public class DetectionInformation {
    /*
    * id
    * */
    private Integer id;
    /*
    * 检测日期  开始 - 结束
    * */
    private String detectionDateStart;
    private String detectionDateEnd;
    /*
    * 检测单位
    * */
    private String detectionUnit;

    /*
    * 状态 1合格 0不合格
    * */
    private Integer status;

    /*
    * 环保号码
    * */
    private String phone;

}
