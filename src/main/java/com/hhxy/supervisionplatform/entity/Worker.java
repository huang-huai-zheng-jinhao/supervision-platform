package com.hhxy.supervisionplatform.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 *
 * </p>
 *
 * @author zjh
 * @since 2023-03-02
 */
public class
Worker implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String name;

    /**
     * 所属机构名称
     */
    private String fOrganizationName;

    /**
     * 所属机构地点
     */
    private String organizationAdress;

    /**
     * 入职时间
     */
    private LocalDate onboardingTime;

    /**
     * 文化程度
     */
    private String acculturation;

    /**
     * 职位
     */
    private String position;

    /**
     * 电话
     */
    private String phone;

    /**
     * 岗位确定编号
     */
    private String posDetDocNum;

    /**
     * 岗位起始确定时间
     */
    private LocalDate posDetDocSTime;

    /**
     * 岗位起始结束时间
     */
    private LocalDate posDetDocETime;

    /**
     * 照片信息
     */
    private String picRes;

    /**
     * 本人照片
     */
    private String selfie;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getfOrganizationName() {
        return fOrganizationName;
    }

    public void setfOrganizationName(String fOrganizationName) {
        this.fOrganizationName = fOrganizationName;
    }
    public String getOrganizationAdress() {
        return organizationAdress;
    }

    public void setOrganizationAdress(String organizationAdress) {
        this.organizationAdress = organizationAdress;
    }
    public LocalDate getOnboardingTime() {
        return onboardingTime;
    }

    public void setOnboardingTime(LocalDate onboardingTime) {
        this.onboardingTime = onboardingTime;
    }
    public String getAcculturation() {
        return acculturation;
    }

    public void setAcculturation(String acculturation) {
        this.acculturation = acculturation;
    }
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getPosDetDocNum() {
        return posDetDocNum;
    }

    public void setPosDetDocNum(String posDetDocNum) {
        this.posDetDocNum = posDetDocNum;
    }
    public LocalDate getPosDetDocSTime() {
        return posDetDocSTime;
    }

    public void setPosDetDocSTime(LocalDate posDetDocSTime) {
        this.posDetDocSTime = posDetDocSTime;
    }
    public LocalDate getPosDetDocETime() {
        return posDetDocETime;
    }

    public void setPosDetDocETime(LocalDate posDetDocETime) {
        this.posDetDocETime = posDetDocETime;
    }
    public String getPicRes() {
        return picRes;
    }

    public void setPicRes(String picRes) {
        this.picRes = picRes;
    }
    public String getSelfie() {
        return selfie;
    }

    public void setSelfie(String selfie) {
        this.selfie = selfie;
    }

    @Override
    public String toString() {
        return "Worker{" +
            "id=" + id +
            ", name=" + name +
            ", fOrganizationName=" + fOrganizationName +
            ", organizationAdress=" + organizationAdress +
            ", onboardingTime=" + onboardingTime +
            ", acculturation=" + acculturation +
            ", position=" + position +
            ", phone=" + phone +
            ", posDetDocNum=" + posDetDocNum +
            ", posDetDocSTime=" + posDetDocSTime +
            ", posDetDocETime=" + posDetDocETime +
            ", picRes=" + picRes +
            ", selfie=" + selfie +
        "}";
    }
}
