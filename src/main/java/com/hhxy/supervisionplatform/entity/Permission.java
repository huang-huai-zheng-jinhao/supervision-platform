package com.hhxy.supervisionplatform.entity;

import lombok.Data;

@Data
public class Permission {
    private Integer id;
    private Integer permissionId;
    private String permissionDesc;
}
