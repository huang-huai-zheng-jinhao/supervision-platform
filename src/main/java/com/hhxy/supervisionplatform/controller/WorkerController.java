package com.hhxy.mybatisplus.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zjh
 * @since 2023-03-02
 */
@Controller
@RequestMapping("/mybatisplus/worker")
public class WorkerController {

}
