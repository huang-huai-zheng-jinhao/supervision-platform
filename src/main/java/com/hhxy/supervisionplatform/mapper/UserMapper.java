package com.hhxy.supervisionplatform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hhxy.supervisionplatform.entity.User;


public interface UserMapper extends BaseMapper<User> {
}
