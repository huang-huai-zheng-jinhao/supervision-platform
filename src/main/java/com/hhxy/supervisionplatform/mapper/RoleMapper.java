package com.hhxy.supervisionplatform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hhxy.supervisionplatform.entity.Role;


public interface RoleMapper extends BaseMapper<Role> {
}
