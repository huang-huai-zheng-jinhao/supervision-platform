package com.hhxy.supervisionplatform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hhxy.supervisionplatform.entity.Worker;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjh
 * @since 2023-03-02
 */
public interface WorkerMapper extends BaseMapper<Worker> {

}
