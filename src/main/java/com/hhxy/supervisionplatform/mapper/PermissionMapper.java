package com.hhxy.supervisionplatform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hhxy.supervisionplatform.entity.Permission;

public interface PermissionMapper extends BaseMapper<Permission> {
}
