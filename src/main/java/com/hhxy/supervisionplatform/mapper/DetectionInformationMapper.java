package com.hhxy.supervisionplatform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hhxy.supervisionplatform.entity.DetectionInformation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DetectionInformationMapper extends BaseMapper<DetectionInformation> {
}
